plugin.filter('wgnFieldsFilterWithLinked', [function() {
    return function(items) {
        var filtered = [];
        angular.forEach(items, function(item) {
            if (item.type === 'text' || item.type === 'text-area' || item.type === 'text-input' || item.type === 'linked') {
                filtered.push(item);
            }
        });

        return filtered;
    };
}])

plugin.filter('wgnFieldsFilter', [function() {
    return function(items) {
        var filtered = [];
        angular.forEach(items, function(item) {
            if (item.type === 'text' || item.type === 'text-area' || item.type === 'text-input') {
                filtered.push(item);
            }
        });

        return filtered;
    };
}])

plugin.filter('wgnFormsFilterChildren', [function() {
    return function(items, parent) {
    	if(!parent || !items){
    		return items;
    	}
    	var filtered = items.filter(function(form){
    		return form.linkedForms && form.linkedForms.filter(function(link){return link.form.id == parent}).length > 0;
    	});
        return filtered;
    };
}])