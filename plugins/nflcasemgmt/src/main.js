/**
 * Plugin Sales wgn Main Controller
 */
plugin.controller('wgnCntl', ['$scope', '$q', '$filter', 'znConfirm', '$window', '$location', '$routeParams', 'znData', 'znMessage', '$firebase', '$templateCache', 'znModal', function($scope, $q, $filter, znConfirm, $window, $location, $routeParams, znData, znMessage, $firebase, $templateCache, znModal) {
    $scope.settings_form_name = 'wgnSettings';
    $scope.orders_form_name = 'wgnOrder';
    $scope.order_direction = 'order';
    $scope.workspace_id = $routeParams.workspace_id;
    $scope.settings_fields = false;
    $scope.settings_data = false;
    $scope.loading = true;
    $scope.selected_data_view = false;
    $scope.selected_data_view_name = false;
    $scope.filter_folders_string = false;

    $scope.checkDate = function(dateString) {

        if (new Date(dateString) > new Date()) {
            return true;
        }
        return false;
    }

    $scope.log_selected = function(item, model) {
        $scope.link_id = item.value;
    }

    $scope.get_date_diffrence = function(date) {
        var first_date = new Date(date);
        var second_date = new Date();
        var timeDiff = Math.abs(second_date.getTime() - first_date.getTime());
        return Math.ceil(timeDiff / (1000 * 3600 * 24));
    }

    $scope.set_selected_data_view = function(view) {
        $scope.total = false;
        if (view) {
            $scope.selected_data_view = view.id;
            $scope.selected_data_view_name = view.name;
            $scope.selected_data_view_object = view;

            $scope.loading_settings = true;
            var promises = [];
            promises = promises.concat($scope.save_plugin_settings('selected_data_view', false));
            promises = promises.concat($scope.save_plugin_settings('selected_data_view_name', false));

            $q.all(promises).then(function() {
                //znMessage('Settings saved', 'saved');
                $scope.load_plugin_data();
                //$window.location.reload();
            });
        } else {
            $scope.selected_data_view = -1;
            $scope.selected_data_view_name = '';
            $scope.selected_data_view_object = null;

            $scope.loading_settings = true;
            var promises = [];
            promises = promises.concat($scope.save_plugin_settings('selected_data_view', false));
            promises = promises.concat($scope.save_plugin_settings('selected_data_view_name', false));

            $q.all(promises).then(function() {
                // znMessage('Settings saved', 'saved');
                $scope.load_plugin_data();
                //$window.location.reload();
            });
        }
    }

    $scope.calculate = function() {

        $scope.total = 0;

        angular.forEach($scope.relatedRecords, function(group, key) {

            angular.forEach($scope.folders, function(folder, key) {
                if (($scope.filter_folders_string && $scope.filter_folders_string.indexOf(folder.id) != -1) || $scope.selected_data_view != -1) {
                    folder.total = 0;
                    if (typeof $scope.folderRecords[folder.id] != 'undefined') {
                        angular.forEach($scope.folderRecords[folder.id][group.id].records, function(record, key) {
                            if (record[$scope.amount_field_name]) {
                                folder.total += parseFloat(record[$scope.amount_field_name]);
                            }

                        });
                    }
                    $scope.total += parseFloat(folder.total);
                }

                if ($scope.selected_data_view == -1) {
                    folder.total = 0;

                    angular.forEach($scope.folderRecords[folder.id][group.id].records, function(record, key) {
                        if (record[$scope.amount_field_name]) {
                            folder.total += parseFloat(record[$scope.amount_field_name]);
                        }

                    });
                    $scope.total += parseFloat(folder.total);
                }

            });

        });

    }

    $scope.refresh = function() {
        $scope.total = false;
        angular.forEach($scope.folders, function(folder, key) {
            folder.total = false;
        });
        $scope.refresh_mode = true;
        $scope.check_plugin_exists();
    }

    $scope.get_all_data_views = function(form) {
        /**
         * Get Data Views List
         */
        znData('DataViews').query({
                workspace: {
                    id: $scope.workspace_id
                },
                form: {
                    id: parseInt(form)
                },
                limit: 100

            },
            function(resp) {
                $scope.all_data_views = resp;
                $scope.set_selected_data_view_object($scope.selected_data_view);

            },
            function(resp) {
                $scope.err = resp;
            }
        );

    }

    $scope.set_selected_data_view_object = function(id) {

        $scope.view_not_belong_to_form = 0;
        angular.forEach($scope.all_data_views, function(view, key) {

            if (view.id === parseInt(id)) {
                $scope.selected_data_view_object = view;
                $scope.selected_data_view = parseInt(id);
                $scope.selected_data_view_name = view.name;
            } else {
                $scope.view_not_belong_to_form++;
            }
        });

    }

    /**
     * Get the current plugin data
     */
    znData('Plugins').get({
            namespace: 'wgn'
        },
        function(resp) {
            $scope.plugin = resp[0];
        },
        function(resp) {
            $scope.err = resp;
        }
    );

    /**
     * Get the currently logged-in user data
     */
    znData('Users').get({
            id: 'me',
        },
        function(resp) {
            $scope.user = resp;
        },
        function(resp) {
            $scope.err = resp;
        }
    );



    /**
     * Wait for plugin and user data and connects to Firebase
     */
    var unbind = $scope.$watchCollection('[plugin, user]', function() {

        // Stop on any errors fetching plugin or user data
        if ($scope.err) {
            return;
        }

        // Checks if both plugin and user data is available to connect
        if ($scope.plugin !== undefined && $scope.user !== undefined) {
            $scope.connect();
            unbind();
        }

    });

    var pluginSettings;
    /**
     * Connects to firebase and authenticate
     */
    $scope.connect = function() {
        
        pluginSettings = new Firebase($scope.plugin.firebaseUrl + $scope.workspace_id);

        // Uses the `$scope.user.id` to make a direct reference to the current user preferences
        var ref = new Firebase($scope.plugin.firebaseUrl + 'preferences/' + $scope.user.id);

        // Uses the `$scope.plugin.firebaseAuthToken` to authenticate the user
        ref.auth($scope.plugin.firebaseAuthToken, function(err, res) {

            if (err) {
                $scope.err = err;
                return;
            }

            // Set auth data
            $scope.auth = res.auth;

            // Set preferences
            $scope.preferences = $firebase(ref).$asObject();

            // Apply changes to the scope
            $scope.$apply();

            // AND GO!
            $scope.check_plugin_exists();

        });
    };

    $scope.$watchCollection('[notesFormField,selectedFormObject]', function() {
        if ($scope.notesFormField && $scope.selectedFormObject) {
            angular.forEach($scope.selectedFormObject.linkedForms, function(item, key) {
                if (item.form.id === parseInt($scope.notesFormField)) {
                    $scope.related_form_id = item.form.id;
                    $scope.related_field_id = 'field' + item.keyField.id;

                }
            });
        }
    });

    $scope.$watchCollection('[all_forms_fields,selectedForm,valueField]', function() {
        if ($scope.selectedForm && $scope.valueField && $scope.all_forms_fields) {
            $scope.dots_field = [];
            angular.forEach($scope.all_forms_fields, function(item, key) {
                if (item.form_id === parseInt($scope.selectedForm) && item.id === parseInt($scope.valueField)) {
                    angular.forEach(item.settings.properties.choices, function(item, key) {
                        $scope.dots_field.push(key);
                    });
                }
            });

        }
    });

    $scope.get_related_records = function() {

    }

    $scope.formsFilter = function(item) {
        return item.name !== $scope.orders_form_name && item.name !== $scope.settings_form_name
    }

    $scope.findFormName = function(id) {
        var name = id;
        angular.forEach($scope.all_forms, function(item, key) {
            if (item.id === id) {
                name = item.name;
            }
        });
        return name;
    };

    $scope.init = function() {
        //            $scope.check_plugin_exists();
    };

    $scope.format_field = function(field) {
        if (typeof(field) == 'object' && field && field.name) {
            var title = field.name.toLowerCase().split(',');
            var firstname = (typeof title[1] != 'undefined') ? title[1] + ' ' : '';
            var lastname = (typeof title[0] != 'undefined') ? title[0] : '';
            return firstname + lastname;
        } else if (typeof(field) == 'object' && field && !field.name) {
            return '';
        }
        return field;
    };

    $scope.check_plugin_exists = function() {

        pluginSettings.once('value', function(snapshot) {
            if (snapshot.val() && snapshot.val()[$scope.settings_form_name]) {
                $scope.settings = snapshot.val();
                $scope.selected_data_view = snapshot.val()[$scope.settings_form_name].selected_data_view;
                $scope.selected_data_view_name = snapshot.val()[$scope.settings_form_name].selected_data_view_name;
                $scope.load_orders();
            } else {
                $scope.loading = false;
                $scope.error_columns = 'Please setup the form you want to present on settings panel';
                $scope.load_all_forms();
                return
            }
            if (snapshot.val() && snapshot.val()[$scope.settings_form_name].nameField && snapshot.val()[$scope.settings_form_name].seasonsField) {
                $scope.load_all_forms();
            } else {
                $scope.loading = false;
                $scope.error_columns = 'Please setup title and description fields on settings panel';
                $scope.load_all_forms();
                return
            }
        })
    };

    $scope.load_all_forms = function() {

        znData('Forms').query({
            'workspace': {
                'id': $scope.workspace_id
            },
            'related': 'fields',
            'limit': 30
        }).then(function(forms) {
            $scope.all_forms = forms;
            $scope.all_forms_ids = [];
            for (var i in forms) {
                $scope.all_forms_ids.push(forms[i].id);
            }
            $scope.load_plugin_data();
            $scope.load_all_forms_fields();
        });

    };

    $scope.load_all_forms_fields = function() {
        $scope.all_forms_fields = [];

        angular.forEach($scope.all_forms, function(form, key) {
            for (var i in form.fields) {
                form.fields[i].form_id = form.id;
            }
            $scope.all_forms_fields = $scope.all_forms_fields.concat(form.fields);
        });

    };
    
    $scope.getPlayerInfo = function(recs){
        
        // This function gets the Player information 
        // for a set of Engagement records (recs)
        // It directly appends the player data to the engagement record
        
        var cmSettings = $scope.settings.wgnSettings;
        var idList = [];
        
        recs.forEach(function(rec){
            idList.push(rec['field' + cmSettings.contactFormField].id);
        })
        
        var idString = idList.join('|');
        
        var query = {"id":idString}
        
        if(idList.length > 0){
            znData('FormRecords').query({formId:cmSettings.contactForm, id:idString, limit:500}, function(players){
                recs.forEach(function(engagement){
                    var player = players.filter(function(player){
                        return player.id == engagement['field' + cmSettings.contactFormField].id
                    })[0];
                    engagement['field' + cmSettings.stateField] = player['field' + cmSettings.stateField]
                    engagement['field' + cmSettings.amountField] = player['field' + cmSettings.amountField]
                    engagement['field' + cmSettings.seasonsField] = player['field' + cmSettings.seasonsField]
                })
            })
        }
        
    }

    $scope.load_orders = function() {
        
        var fieldOrder = pluginSettings.child($scope.orders_form_name);
        fieldOrder.once('value', function(snapshot) {
            if (snapshot.val()) {
                $scope.orders = snapshot.val();
            }
        })
    };

    $scope.load_plugin_data = function() {
        $scope.load_settings_records();
        $scope.load_related_records();
    };

    $scope.load_related_records = function() {

        $scope.textarea_mode = true;
        $scope.$watchCollection('[selectedFormObject,seasonsField]', function() {
            if ($scope.selectedFormObject && $scope.seasonsField) {
                angular.forEach($scope.selectedFormObject.linkedForms, function(item, key) {
                    if (item.keyField.id == parseInt($scope.seasonsField)) {
                        $scope.textarea_mode = false;
                        $scope.related_records_array = [];
                        znData('FormRecords').query({
                            'formId': item.form.id
                        }).then(function(result) {
                            if (result != null && result != '') {
                                for (i in result) {
                                    $scope.related_records_array.push({
                                        "value": result[i].id,
                                        "name": result[i].name
                                    });
                                }
                            }


                        });
                    }
                });
            }
        });

    }


    $scope.load_settings_records = function() {

        pluginSettings.once('value', function(snapshot) {

            

            if (snapshot.val()) {
                $scope.groupingForm = snapshot.val().groupingForm;
            }
            if (snapshot.val() && snapshot.val()[$scope.settings_form_name]) {
                $scope.settings = snapshot.val();
                var selectedForm = snapshot.val()[$scope.settings_form_name].selectedForm;
                $scope.selectedForm = selectedForm;
                $scope.get_all_data_views(selectedForm);

                $scope.load_selected_form();
            } else {
                $scope.load_defaults();
            }
            if ($scope.all_forms_ids.indexOf(parseInt($scope.groupingForm)) == -1) {
                $scope.groupingForm = 0;
                $scope.groupingField = 0;
                $scope.groupingEnabled = '0';

            }
        })
    };

    $scope.load_selected_form = function() {
        if ($scope.selectedForm) {
            if ($scope.selectedForm && $scope.all_forms_ids.indexOf(parseInt($scope.selectedForm)) == -1) {
                $scope.selectedForm = $scope.all_forms[0].id;
            }
            for (var i in $scope.all_forms) {
                if ($scope.all_forms[i].id == $scope.selectedForm) {
                    $scope.selectedFormObject = $scope.all_forms[i];
                    for (var j in $scope.selectedFormObject.linkedForms) {
                        $scope.selectedFormObject.linkedForms[j].form.name = $scope.findFormName($scope.selectedFormObject.linkedForms[j].form.id);
                    }
                }
            }
            $scope.load_selected_form_fields();
        } else {
            $scope.load_defaults();
        }
    };

    $scope.load_defaults = function() {
        //            $scope.selectedForm = $scope.all_forms[0].id;
        $scope.selectedForm = "1";

        $scope.selectedFormObject = $scope.all_forms[0];
        for (var j in $scope.selectedFormObject.linkedForms) {
            $scope.selectedFormObject.linkedForms[j].form.name = $scope.findFormName($scope.selectedFormObject.linkedForms[j].form.id);
        }
        $scope.load_selected_form_fields();

    };

    $scope.load_selected_form_folders = function() {

        var params = {
            id: $scope.selectedForm,
            related: 'folders'
        };
        return znData('Forms').query(params).then(function(response) {
            $scope.form = response[0];
            $scope.folders = $scope.form.folders;
            $scope.folders.push($scope.folders.shift());
            $scope.loadRecords();
        });
    };

    $scope.load_selected_form_fields = function() {
        $scope.all_form_fields_ids = [];
        return znData('FormFields').query({
            'formId': $scope.selectedForm
        }, {}, function(fields) {
            $scope.all_form_fields = [];
            angular.forEach(fields, function(field, index) {
                if (field.type == 'text' || field.type == 'text-input' || field.type == 'text-area' || field.type == 'linked') {
                    $scope.all_form_fields_ids.push(field.id);
                    $scope.all_form_fields.push(field);
                }
                if (field.id == $scope.nameField) {
                    $scope.title_placeholder = field.label;
                }
                if (field.id == $scope.seasonsField) {
                    $scope.description_placeholder = field.label;
                }
            });
            $scope.load_selected_form_folders();
        });
    };


    // Selected Form ID
    $scope.formId = 0;

    // Selected Form Folders
    $scope.folders = [];

    // Records Indexed by Folder
    $scope.folderRecords = {};

    // Show Add Folder Flag
    $scope.showAddFolder = false;

    // Add Folder Name
    $scope.addFolderName = null;

    // Selected Folder to Edit
    $scope.editFolder = {
        id: null,
        name: null
    };

    // Sortable Options
    $scope.sortableOptions = {
        connectWith: "ul.records-container",
        items: "li.record",
        cancel: '.input-holder',
        stop: function(event, ui) {
            var to_folder = angular.element(ui.item).parent().data('id');
            var from_folder = angular.element(event.target).data('id');

            var newOrder = {}

            var folderRecordsNewOrder = document.getElementsByClassName('records-container');
            angular.forEach(folderRecordsNewOrder, function(folder) {
                var records = folder.getElementsByTagName('li');
                angular.forEach(records, function(record, currentOrder) {
                    var recId = record.getAttribute('data-id');
                    if (recId !== null) {
                        newOrder[recId] = records.length - currentOrder; //reverse the sort order
                    }
                })
            })

            //Firebase Settings
            pluginSettings.child($scope.orders_form_name).set(newOrder)
            $scope.orders = newOrder;

            angular.forEach($scope.folders, function(folder) {

                if (typeof $scope.folderRecords[folder.id] != 'undefined' && typeof $scope.folderRecords[folder.id][ui.item.data('group')] != 'undefined') {
                    var records = $scope.folderRecords[folder.id][ui.item.data('group')].records;
                } else if (typeof $scope.folderRecords[folder.id] != 'undefined') {
                    $scope.folderRecords[folder.id][ui.item.data('group')] = {};
                    $scope.folderRecords[folder.id][ui.item.data('group')].records = [];
                    var records = $scope.folderRecords[folder.id][ui.item.data('group')].records;
                }

                angular.forEach(records, function(record, index) {
                    
                    var thisOrder = $scope.orders[record.id];

                    if (record.id == ui.item.data('id') && to_folder != from_folder && typeof to_folder != 'undefined') {
                        znData('FormRecords').save({
                            formId: $scope.selectedForm,
                            id: record.id,
                            order_num: thisOrder
                        }, {
                            folder: {
                                id: parseInt(to_folder)
                            }
                        }, function(response) {}, function(e) {
                            znMessage('Error moving record', 'error');
                        });
                    }
                    var params = {};
                });
            });
        }
    };

    $scope.sortableFoldersOptions = {
        connectWith: ".columns",
        items: ".column",
        cancel: ".no-sort,.folder-name-input,.input-holder",
        axis: "x",
        stop: function(event, ui) {
            angular.forEach($scope.folders, function(folder, key) {
                if (folder && folder.id !== 0) {
                    znData('FormFolders').save({
                        formId: $scope.selectedForm,
                        id: folder.id
                    }, {
                        'order': key + 5
                    }, function(returnedFolder) {
                        folder = returnedFolder;

                    });
                }
            });

        }
    };

    /**
     * Load Records by Form Folders
     */
    $scope.loadRecords = function() {
        pluginSettings.once('value', function(snapshot) {
            if (snapshot.val() && snapshot.val()[$scope.settings_form_name]) {
                $scope.nameField = snapshot.val()[$scope.settings_form_name].nameField || null;
                $scope.seasonsField = snapshot.val()[$scope.settings_form_name].seasonsField || null;
                $scope.assignToField = snapshot.val()[$scope.settings_form_name].assignToField || null;
                $scope.enabledFolders = snapshot.val()[$scope.settings_form_name].enabledFolders;
                $scope.amountField = snapshot.val()[$scope.settings_form_name].amountField || null;
                $scope.valueField = snapshot.val()[$scope.settings_form_name].valueField || null;
                $scope.lastActivityField = snapshot.val()[$scope.settings_form_name].lastActivityField || null;
                $scope.followUpField = snapshot.val()[$scope.settings_form_name].followUpField || null;
                
                $scope.notesFormField = snapshot.val()[$scope.settings_form_name].notesFormField || null;
                $scope.orders_id_field = snapshot.val()[$scope.orders_form_name];
                $scope.notesEngagementField = snapshot.val()[$scope.settings_form_name].notesEngagementField || null;
                $scope.notesNotesField = snapshot.val()[$scope.settings_form_name].notesNotesField || null;
                $scope.notesURLField = snapshot.val()[$scope.settings_form_name].notesURLField || null;
                $scope.notesDateField = snapshot.val()[$scope.settings_form_name].notesDateField || null;
                $scope.notesSubjectField = snapshot.val()[$scope.settings_form_name].notesSubjectField || null;
                $scope.stateField = snapshot.val()[$scope.settings_form_name].stateField || null;
                $scope.field1Field = snapshot.val()[$scope.settings_form_name].field1Field || null;
                $scope.field2Field = snapshot.val()[$scope.settings_form_name].field2Field || null;
                
                $scope.field3Field = snapshot.val()[$scope.settings_form_name].field3Field || null;
                $scope.grantFormField = snapshot.val()[$scope.settings_form_name].grantFormField || null;
                
                

                $scope.recentField = parseInt(snapshot.val()[$scope.settings_form_name].recentField) || null;
                $scope.currentField = parseInt(snapshot.val()[$scope.settings_form_name].currentField) || null;
                $scope.overdueField = parseInt(snapshot.val()[$scope.settings_form_name].overdueField) || null;

                $scope.contactForm = parseInt(snapshot.val()[$scope.settings_form_name].contactForm) || null;
                $scope.contactFormField = parseInt(snapshot.val()[$scope.settings_form_name].contactFormField) || null;

                $scope.approvalField = parseInt(snapshot.val()[$scope.settings_form_name].approvalField) || null;
                
            }

            //angular.forEach($scope.all_form_fields, function(item) {
                //if (item.id == parseInt($scope.nameField)) {
                if(!$scope.nameField){
                    $scope.title_field_not_set = true;
                }
                //if (item.id == parseInt($scope.seasonsField)) {
                if(!$scope.seasonsField){
                    $scope.description_field_not_set = true;
                }
            //});

            $scope.view_folders = [];
            apply_folder_filter = function(filter){
                angular.forEach(filter, function(andOr,aoKey){
                    andOr.forEach(function(item,key){
                        if(item.hasOwnProperty('attribute')){
                            if(item.attribute == 'folder.id'){
                                if(item.prefix != 'not'){
                                    showFolders.push(Number(item.value));
                                } else {
                                    hideFolders.push(Number(item.value));
                                }
                            }

                        } else {
                            apply_folder_filter(item);
                        }
                    })
                    
                })
            }

            if ($scope.selected_data_view_object && $scope.selected_data_view_object.filter) {
                
                var showFolders = [];
                var hideFolders = [];



                apply_folder_filter($scope.selected_data_view_object.filter);

                if(showFolders.length > 0 || hideFolders.length > 0){
                    if(showFolders.length > 0 && hideFolders.length > 0){
                        //hide all, show showFolders
                        $scope.folders.forEach(function(folder){
                            folder.show = false;
                            if(showFolders.indexOf(folder.id) > -1){
                                folder.show = true;
                            }
                        })
                    } else if(showFolders.length > 0){
                        //only show the defined folders
                        $scope.folders.forEach(function(folder){
                            folder.show = false;
                            if(showFolders.indexOf(folder.id) > -1){
                                folder.show = true;
                            }
                        })
                    } else {
                        //hideFolders only
                        $scope.folders.forEach(function(folder){
                            folder.show = true;
                            if(hideFolders.indexOf(folder.id) > -1){
                                folder.show = false;
                            }
                        })
                    }
                } else {

                }
                
            }              

            for (var i in $scope.view_folders) {
                $scope.view_folders[i] = parseInt($scope.view_folders[i]);
            }

            var queue = [];
            var promises = [];
            $scope.users_objects = {};
            $scope.folderRecords = {};
            $scope.title_field_name = 'field' + $scope.nameField;
            $scope.description_field_name = 'field' + $scope.seasonsField;
            $scope.value_field_name = 'field' + $scope.valueField;
            $scope.amount_field_name = 'field' + $scope.amountField;
            $scope.state_field_name = 'field' + $scope.stateField;
            
            $scope.interactions_form_field_name = 'field' + $scope.notesFormField;
            $scope.last_activity_field_name = 'field' + $scope.lastActivityField;
            $scope.follow_up_field_name = 'field' + $scope.opportunityFollowUpField;

            // if (typeof $scope.enabledFolders == 'undefined' || $scope.enabledFolders == 'FormChanged') {
            //     $scope.enabledFolders = '';
            //     angular.forEach($scope.folders, function(folder) {
            //         $scope.enabledFolders += '|' + folder.id + '|';
            //     });
            // }

            $scope.relatedRecords = {};
            if ($scope.groupingEnabled !== '0') {
                $scope.groupingFieldName = 'field' + $scope.groupingField;
                promises.push(znData('FormRecords').query({
                    formId: $scope.groupingForm
                }).then(function(relatedRecords) {
                    angular.forEach(relatedRecords, function(record) {
                        $scope.relatedRecords[record.id] = record;
                        $scope.relatedRecords[record.id].name = record[$scope.groupingFieldName];
                    });
                    $scope.relatedRecords['notFound'] = {
                        id: 'notFound',
                        name: 'No group found'
                    };
                }));
            } else {
                $scope.relatedRecords['notFound'] = {
                    id: 'notFound',
                    name: ''
                };
            }
            $q.all(promises).then(function() {

                var params = {
                    formId: $scope.selectedForm,
                    folder: {}
                };
                /*
                                if (!(($scope.nameField && $scope.all_form_fields_ids.indexOf(parseInt($scope.nameField)) > -1) 
                                    && ($scope.seasonsField && $scope.all_form_fields_ids.indexOf(parseInt($scope.seasonsField)) > -1))) {
                                    $scope.error_columns = 'Please setup title and description fields on settings panel';
                                }

                                if ($scope.selectedForm && $scope.all_forms_ids.indexOf(parseInt($scope.selectedForm)) == -1) {
                                    $scope.error_columns = 'Please setup the form you want to present on settings panel';
                                }
                */
                var viewFilterClone = ($scope.selected_data_view_object) ? angular.copy($scope.selected_data_view_object.filter) : false;
                angular.forEach($scope.folders, function(folder) {

                    if ($scope.view_folders.indexOf(folder.id) != -1 || $scope.view_folders.length == 0) {
                        $scope.folderRecords[folder.id] = {};
                        // params = angular.extend(params, angular.copy($scope.filters));
                        //params = $scope.filters || {};
                        $scope.folder_set = false;

                        if ($scope.view_folders.length !== 0) {


                            if ($scope.selected_data_view_object && $scope.selected_data_view_object.filter && $scope.selected_data_view_object.filter.and) {
                                angular.forEach($scope.selected_data_view_object.filter.and, function(item) {

                                    if (item.attribute == 'folder.id') {
                                        if (!$scope.filter_folders_string) { $scope.filter_folders_string = item.value; }

                                        if ($scope.filter_folders_string.indexOf(folder.id) != -1) {
                                            item.value = folder.id;

                                        } else {
                                            delete item;
                                        }
                                        $scope.folder_set = true;
                                    }
                                });
                            }

                            if ($scope.selected_data_view_object && $scope.selected_data_view_object.filter && $scope.selected_data_view_object.filter.or) {
                                angular.forEach($scope.selected_data_view_object.filter.or, function(item) {
                                    if (item.attribute == 'folder.id') {
                                        if (!$scope.filter_folders_string) { $scope.filter_folders_string = item.value; }

                                        if ($scope.filter_folders_string.indexOf(folder.id) != -1) {
                                            item.value = folder.id;

                                        } else {
                                            delete item;
                                        }
                                        $scope.folder_set = true;
                                    }
                                });
                            }
                        }
                        if (!$scope.folder_set) {
                            if ($scope.selected_data_view_object && $scope.selected_data_view_object.filter && $scope.selected_data_view_object.filter.or) {
                                $scope.selected_data_view_object.filter.or.push({
                                    'attribute': 'folder.id',
                                    'prefix': '',
                                    'value': folder.id
                                })
                            } else if ($scope.selected_data_view_object && $scope.selected_data_view_object.filter && $scope.selected_data_view_object.filter.and) {
                                $scope.selected_data_view_object.filter.and.push({
                                    'attribute': 'folder.id',
                                    'prefix': '',
                                    'value': folder.id
                                })
                            }
                        }


                        if ($scope.selected_data_view_object && $scope.selected_data_view_object.filter === null) {
                            $scope.filter = {};
                        } else {
                            $scope.filter = ($scope.selected_data_view_object) ? JSON.stringify($scope.selected_data_view_object.filter) : {};
                        }

                        params.filter = $scope.filter;

                        if (!$scope.folder_set) {
                            if ($scope.selected_data_view_object && $scope.selected_data_view_object.filter && $scope.selected_data_view_object.filter.or) {
                                $scope.selected_data_view_object.filter.or.pop();
                            } else if ($scope.selected_data_view_object && $scope.selected_data_view_object.filter && $scope.selected_data_view_object.filter.and) {
                                $scope.selected_data_view_object.filter.and.pop();
                            }
                        }
                        //if($scope.selected_data_view_object){$scope.selected_data_view_object.filter = viewFilterClone;}

                        if ($scope.view_folders.length == 0) {
                            params.folder.id = folder.id;
                        }
                        params.formId = $scope.selectedForm;

                        params.limit = 200;


                        var request = znData('FormRecords').query(params).then(function(response) {

                            var members_promises = [];
                            if ($scope.assignToField != '0') {
                                $scope.assignToFieldName = "field" + $scope.assignToField;
                                angular.forEach(response, function(record, key) {
                                    if (record[$scope.assignToFieldName] && typeof $scope.users_objects[record[$scope.assignToFieldName].id] === 'undefined') {
                                        $scope.users_objects[record[$scope.assignToFieldName].id] = {};
                                        members_promises.push(znData('Users').get({
                                            id: record[$scope.assignToFieldName].id
                                        }).then(function(user) {
                                            $scope.users_objects[record[$scope.assignToFieldName].id] = user;
                                        }));
                                    }
                                });
                            }

                            //Cycle through the order settings, set order on the record
                            angular.forEach($scope.orders, function(order, key) {
                                for (var j in response) {
                                    if (response[j].id == key) {
                                        response[j].order_num = order;
                                    }
                                }
                            })

                            if ($scope.groupingEnabled !== '0') {

                                angular.forEach($scope.selectedFormObject.linkedForms, function(form, key) {
                                    if (form.form.id === parseInt($scope.groupingForm)) {
                                        $scope.groupingFieldId = form.keyField.id;
                                        $scope.groupingFieldName = "field" + $scope.groupingFieldId;
                                    }
                                });

                                angular.forEach(response, function(record, key) {
                                    
                                    var key = record[$scope.groupingFieldName].id;
                                    if (!key) {
                                        key = 'notFound';
                                        $scope.folderRecords[folder.id][key] = {
                                            'name': 'No group found'
                                        };
                                        $scope.folderRecords[folder.id][key].records = [];
                                    }
                                    if (typeof($scope.folderRecords[folder.id][key]) == "undefined") {
                                        if (typeof($scope.relatedRecords[key]) == "undefined") {
                                            $scope.folderRecords[folder.id][key] = {
                                                'name': 'No group found'
                                            };
                                        } else {
                                            $scope.folderRecords[folder.id][key] = {
                                                'name': $scope.relatedRecords[key].name
                                            };
                                        }
                                        $scope.folderRecords[folder.id][key].records = [];
                                    }
                                    $scope.folderRecords[folder.id][key].records.push(record);
                                    $scope.folderRecords[folder.id][key].records.sort(function(a, b) {
                                        return b.order_num - a.order_num;
                                    });
                                });
                            } else {
                                key = 'notFound';
                                $scope.folderRecords[folder.id][key] = {
                                    'name': ''
                                };
                                $scope.folderRecords[folder.id][key].records = response;
                                $scope.folderRecords[folder.id][key].records.sort(function(a, b) {
                                    return a.order_num < b.order_num;
                                });
                            }
                            
                            $scope.getPlayerInfo(response);
                            
                        });
                        queue.push(request);

                    }
                });
                
                
                
                $q.all(queue).then(function(){
                    
                    $scope.refresh_mode = false;
                    $scope.loading = false;
                    
                })
                

            });
        })
    };
    /**
     * Add Folder
     */
    $scope.addFolder = function(folder) {

        var params = {
            formId: $scope.selectedForm
        };

        var data = {
            name: folder,
            form: {
                id: $scope.selectedForm
            }
        };
        // Save New Folder
        return znData('FormFolders').save(params, data, function(folder) {
            // Close Add Column
            $scope.add_folder = !$scope.add_folder;
            $scope.folderRecords[folder.id] = [];
            $scope.enabledFolders += "|" + folder.id + "|";
            $scope.folder_name = null;
            // Append New Folder to Folders List
            var last_folder = $scope.folders.pop();
            $scope.folders.push(folder);
            $scope.folders.push(last_folder);
        }, function(e) {
            znMessage('Error creating folder', 'error');
        });
    };

    $scope.deleteFolder = function(folder) {
        var params = {
            formId: $scope.selectedForm,
            id: folder.id
        };

        return znData('FormFolders').delete(params, function() {
            delete $scope.folderRecords[folder.id];
            delete $scope.folders[$scope.folders.indexOf(folder)];
        }, function(e) {
            znMessage('Error deleting folder', 'error');
        });
    };

    /**
     * Save Edit Folder
     */
    $scope.editFolder = function(folder) {
        var params = {
            formId: $scope.selectedForm,
            id: folder.id
        };

        var data = {
            name: folder.name,
            form: {
                id: $scope.selectedForm
            }
        };
        // Save Folder
        return znData('FormFolders').save(params, data, function(response) {
            // Close Edit Folder
            folder.edit_folder = !folder.edit_folder;
        }, function(e) {
            znMessage('Error saving folder', 'error');
        });

    };

    $scope.selectFolder = function(folder_id) {
        if ($scope.enabledFolders.indexOf('|' + folder_id + "|") < 0) {
            $scope.enabledFolders += '|' + folder_id + "|";
        } else {
            $scope.enabledFolders = $scope.enabledFolders.replace('|' + folder_id + "|", '');
        }

    };



    $scope.save_field = function(group, folder, name, description) {
        $scope.add_loading = true;
        var data = {};
        data[$scope.title_field_name] = name;
        if ($scope.textarea_mode) {
            data[$scope.description_field_name] = description;
        } else {
            data[$scope.description_field_name] = $scope.link_id;
        }

        data.folder = {};
        data.folder.id = folder;
        data.form = {};
        data.form.id = $scope.selectedForm;
        data.validate = false;
        znData('FormRecords').save({
            'formId': $scope.selectedForm
        }, data, function(record) {
            record.folder = data.folder;
            if (typeof $scope.folderRecords[folder][group] == 'undefined') {
                $scope.folderRecords[folder][group] = {};
                $scope.folderRecords[folder][group].records = [];
            }
            if (typeof $scope.folderRecords[folder][group].records == 'undefined') {
                $scope.folderRecords[folder][group].records = [];
            }
            $scope.folderRecords[folder][group].records.push(record);
            $scope.add_loading = false;
        }, function(e) {
            $scope.add_loading = false;
            znMessage('Error saving card.   Please check that the form does not have required fields that are not being entered here', 'error');
        });
    };
    $scope.openRecord = function(record) {
        $location.search('record', $scope.selectedForm + '.' + record.id);
    };
    $scope.add_record = function(record) {
        $location.search('record', $scope.selectedForm + '.add');
    };
    $scope.isDescendant = function(parent, child) {
        var node = child.parentNode;
        while (node != null) {
            if (node == parent) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    };
    angular.element(document.body).on('click', function(e) {
        if (!$scope.isDescendant(angular.element(document.body).find('.settings-container')[0], angular.element(e.target)[0])) {
            $scope.settings = false;
        }
        if (!$scope.isDescendant(angular.element(document.body).find('.columns-container')[0], angular.element(e.target)[0])) {
            $scope.columns = false;
        }
    });

    $scope.selectedFormChanged = function() {
        $scope.load_selected_form_folders();
        $scope.enabledFolders = 'FormChanged';
        $scope.save_plugin_settings('enabledFolders', false);
        angular.forEach($scope.folders, function(folder) {
            $scope.enabledFolders += '|' + folder.id + '|';
        });
        angular.forEach($scope.all_forms, function(form) {
            if (form.id == parseInt($scope.selectedForm)) {
                $scope.selectedFormObject = form;
            }
        });

        for (var j in $scope.selectedFormObject.linkedForms) {
            $scope.selectedFormObject.linkedForms[j].form.name = $scope.findFormName($scope.selectedFormObject.linkedForms[j].form.id);
        }
    }

    $scope.save_plugin_settings_all = function() {
        $scope.loading_settings = true;
        var promises = [];
        promises = promises.concat($scope.save_plugin_settings('nameField', false));
        promises = promises.concat($scope.save_plugin_settings('seasonsField', false));
        promises = promises.concat($scope.save_plugin_settings('selectedForm', false));
        promises = promises.concat($scope.save_plugin_settings('groupingForm', false));
        promises = promises.concat($scope.save_plugin_settings('groupingField', false));
        promises = promises.concat($scope.save_plugin_settings('groupingEnabled', false));
        promises = promises.concat($scope.save_plugin_settings('assignToField', false));
        promises = promises.concat($scope.save_plugin_settings('notesDateField', false));
        promises = promises.concat($scope.save_plugin_settings('notesURLField', false));
        promises = promises.concat($scope.save_plugin_settings('notesEngagementField', false));
        promises = promises.concat($scope.save_plugin_settings('notesNotesField', false));
        promises = promises.concat($scope.save_plugin_settings('notesSubjectField', false));
        promises = promises.concat($scope.save_plugin_settings('notesFormField', false));
        promises = promises.concat($scope.save_plugin_settings('followUpField', false));
        promises = promises.concat($scope.save_plugin_settings('stateField', false));
        promises = promises.concat($scope.save_plugin_settings('field1Field', false));
        promises = promises.concat($scope.save_plugin_settings('field2Field', false));
       
        promises = promises.concat($scope.save_plugin_settings('field3Field', false));
        promises = promises.concat($scope.save_plugin_settings('grantFormField', false));
        
        

        promises = promises.concat($scope.save_plugin_settings('recentField', false));
        promises = promises.concat($scope.save_plugin_settings('currentField', false));
        promises = promises.concat($scope.save_plugin_settings('overdueField', false));
        
        promises = promises.concat($scope.save_plugin_settings('contactForm', false));
        promises = promises.concat($scope.save_plugin_settings('contactFormField', false));

        promises = promises.concat($scope.save_plugin_settings('approvalField', false));

        $q.all(promises).then(function() {
            znMessage('Settings saved, now refreshing!', 'saved');
            $window.location.reload();
        });
    };
    $scope.save_plugin_settings_folders = function() {
        $scope.loading_columns = true;
        var promises = [];
        promises = promises.concat($scope.save_plugin_settings('enabledFolders', false));
        $q.all(promises).then(function() {
            znMessage('Columns saved, now refreshing!', 'saved');
            $scope.loading_columns = false;
            $scope.columns = !$scope.columns;
        });
    };
    $scope.save_plugin_settings = function(fieldname, reload) {
        var promises = [];
        var formSettings;
        //$scope.name_field = $scope.settings_fields_array[0];
        //$scope.value_field = $scope.settings_fields_array[1];
        var count = 0;

        pluginSettings.once('value', function(snapshot) {
            if (snapshot) {
                formSettings = snapshot.val()[$scope.settings_form_name];
                $scope.settings = snapshot.val()[$scope.settings_form_name];
            }
        })

        //Firebase update Settings
        if ($scope[fieldname] === undefined) {
            $scope[fieldname] = '';
        }
        var temp_object = {};
        temp_object[fieldname] = $scope[fieldname];
        pluginSettings.child($scope.settings_form_name).update(temp_object);

    };
    $scope.show_quick_add_form = function() {

        if (($scope.nameField && $scope.all_form_fields_ids.indexOf(parseInt($scope.nameField)) > -1) && ($scope.seasonsField && $scope.all_form_fields_ids.indexOf(parseInt($scope.seasonsField)) > -1)) {
            this.add_record = !this.add_record;
        } else {
            znMessage('Please setup title and description fields on settings panel', 'error');
        }
    };

    $scope.view_interactions = function(record) {
        znModal({
            title: 'My Modal Dialog',
            template: $templateCache.get('wgn-view-interactions.html'),
            scope: {
                'record': record,
                'rootScope': $scope
            },
            classes: 'view-interactions-modal',
            btns: {
                'Add new note': {
                    primary: true
                }
            }
        });
    };
    $scope.view_grants = function(record, shouldFilter) {
        shouldFilter = shouldFilter || false;
        znModal({
            title: 'My Modal Dialog',
            template: $templateCache.get('wgn-view-grants.html'),
            scope: {
                'record': record,
                'rootScope': $scope,
                'shouldFilter': shouldFilter,
                'forms': $scope.all_forms
            },
            classes: 'view-interactions-modal'
        });
    };

    $scope.add_interaction = function(record) {
        znModal({
            title: 'My Modal Dialog',
            template: $templateCache.get('wgn-add-interaction.html'),
            scope: {
                'record': record,
                'rootScope': $scope
            },
            classes: 'add-interaction-modal',
            btns: {
                'Save': {
                    primary: true
                }
            },
            unique: false
        });
    }

    $scope.view_opportunity_info = function(record) {
        znModal({
            title: 'My Modal Dialog',
            template: $templateCache.get('wgn-view-opportunity.html'),
            scope: {
                'record': record,
                'rootScope': $scope
            },
            btns: {
                'Save': {
                    primary: true
                }
            },
            classes: 'view-opportunity-modal',
            unique: false
        });
    }
    $scope.snooze = function(record) {
        znModal({
            title: 'My Modal Dialog',
            template: $templateCache.get('wgn-add-snooze.html'),
            scope: {
                'record': record,
                'rootScope': $scope
            },
            classes: 'snooze-modal',
            btns: {
                'Snooze': {
                    primary: true
                }
            },
            unique: true
        });
    }


    return $scope.init();
}])

