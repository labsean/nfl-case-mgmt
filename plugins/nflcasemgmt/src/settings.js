plugin.controller('wgnSettingsCntl', ['$scope', '$q', '$filter', 'znConfirm', '$window', '$location', '$routeParams', 'znData', 'znMessage', '$firebase', function($scope, $q, $filter, znConfirm, $window, $location, $routeParams, znData, znMessage, $firebase) {
    $scope.settings_form_name = 'wgnSettings';
    $scope.orders_form_name = 'wgnOrder';
    $scope.order_direction = 'order';
    $scope.workspace_id = $routeParams.workspace_id;
    $scope.settings_fields = false;
    $scope.settings_data = false;
    $scope.loading = true;


    /**
     * Get the current plugin data
     */
    znData('Plugins').get({
            namespace: 'wgn'
        },
        function(resp) {
            $scope.plugin = resp[0];
        },
        function(resp) {
            $scope.err = resp;
        }
    );

    /**
     * Get the currently logged-in user data
     */
    znData('Users').get({
            id: 'me',
        },
        function(resp) {
            $scope.user = resp;
        },
        function(resp) {
            $scope.err = resp;
        }
    );

    /**
     * Wait for plugin and user data and connects to Firebase
     */
    var unbind = $scope.$watchCollection('[plugin, user]', function() {

        // Stop on any errors fetching plugin or user data
        if ($scope.err) {
            return;
        }

        // Checks if both plugin and user data is available to connect
        if ($scope.plugin !== undefined && $scope.user !== undefined) {
            $scope.connect();
            unbind();
        }

    });

    var pluginSettings;

    /**
     * Connects to firebase and authenticate
     */
    $scope.connect = function() {
        pluginSettings = new Firebase($scope.plugin.firebaseUrl + $scope.workspace_id);
        // Uses the `$scope.user.id` to make a direct reference to the current user preferences
        var ref = new Firebase($scope.plugin.firebaseUrl + $scope.user.id);
        // Uses the `$scope.plugin.firebaseAuthToken` to authenticate the user
        pluginSettings.auth($scope.plugin.firebaseAuthToken, function(err, res) {

            if (err) {
                $scope.err = err;
                return;
            }

            // Set auth data
            $scope.auth = res.auth;

            // Set preferences
            $scope.preferences = $firebase(ref).$asObject();
            
            // Apply changes to the scope
            $scope.$apply();

            // AND GO!
            $scope.check_plugin_exists();

        });
    };

    $scope.formsFilter = function(item) {
        return item.name !== $scope.orders_form_name && item.name !== $scope.settings_form_name
    }

    $scope.findFormName = function(id) {
        var name = id;
        angular.forEach($scope.all_forms, function(item, key) {
            if (item.id === id) {
                name = item.name;
            }
        });
        return name;
    };

    $scope.init = function() {
        //            $scope.check_plugin_exists();
    };


    $scope.format_field = function(field) {
        if (typeof(field) == 'object' && field && field.name) {
            return field.name;
        } else if (typeof(field) == 'object' && field && !field.name) {
            return '';
        }
        return field;
    };

    $scope.check_plugin_exists = function() {
        pluginSettings.once('value', function(snapshot) {
            if (snapshot.val() && snapshot.val()[$scope.settings_form_name]) {
                //$scope.load_orders();
            } else {
                $scope.loading = false;
                $scope.error_columns = 'Please setup the form you want to present on settings panel';
                $scope.load_all_forms();
                return
            }
            if (snapshot.val() && snapshot.val()[$scope.settings_form_name].nameField && snapshot.val()[$scope.settings_form_name].seasonsField) {
                $scope.load_all_forms();
            } else {
                $scope.loading = false;
                $scope.error_columns = 'Please setup title and description fields on settings panel';
                $scope.load_all_forms();
                return
            }
        })
    };

    $scope.load_all_forms = function() {

        znData('Forms').query({
            'workspace': {
                'id': $scope.workspace_id
            },
            'related': 'fields',
            'limit': 30
        }).then(function(forms) {
            $scope.all_forms = forms;
            $scope.all_forms_ids = [];
            for (var i in forms) {
                $scope.all_forms_ids.push(forms[i].id);
            }
            $scope.load_plugin_data();
            $scope.load_all_forms_fields();
        });
    };

    $scope.load_all_forms_fields = function() {
        $scope.all_forms_fields = [];
        angular.forEach($scope.all_forms, function(form, key) {
            for (var i in form.fields) {
                form.fields[i].form_id = form.id;
            }
            $scope.all_forms_fields = $scope.all_forms_fields.concat(form.fields);
        });
    };



    $scope.load_plugin_data = function() {
        $scope.load_settings_records();

    };

    $scope.load_settings_records = function() {
        pluginSettings.once('value', function(snapshot) {

            if (snapshot.val() && snapshot.val()[$scope.settings_form_name]) {
                var selectedForm = snapshot.val()[$scope.settings_form_name].selectedForm;
                $scope.selectedForm = selectedForm;
                $scope.nameField = snapshot.val()[$scope.settings_form_name].nameField;
                $scope.seasonsField = snapshot.val()[$scope.settings_form_name].seasonsField;
                $scope.assignToField = snapshot.val()[$scope.settings_form_name].assignToField;
                $scope.amountField = snapshot.val()[$scope.settings_form_name].amountField;
                $scope.valueField = snapshot.val()[$scope.settings_form_name].valueField;
                $scope.lastActivityField = snapshot.val()[$scope.settings_form_name].lastActivityField;
                $scope.followUpField = snapshot.val()[$scope.settings_form_name].followUpField;
                $scope.notesFormField = snapshot.val()[$scope.settings_form_name].notesFormField;
                $scope.notesEngagementField = snapshot.val()[$scope.settings_form_name].notesEngagementField || null;
                $scope.notesNotesField = snapshot.val()[$scope.settings_form_name].notesNotesField || null;
                $scope.notesURLField = snapshot.val()[$scope.settings_form_name].notesURLField || null;
                $scope.notesDateField = snapshot.val()[$scope.settings_form_name].notesDateField || null;
                $scope.notesSubjectField = snapshot.val()[$scope.settings_form_name].notesSubjectField || null;

                $scope.stateField = snapshot.val()[$scope.settings_form_name].stateField || null;
                $scope.field1Field = snapshot.val()[$scope.settings_form_name].field1Field || null;
                $scope.field2Field = snapshot.val()[$scope.settings_form_name].field2Field || null;
               
                $scope.field3Field = snapshot.val()[$scope.settings_form_name].field3Field || null;
                $scope.grantFormField = snapshot.val()[$scope.settings_form_name].grantFormField || null;
                
                

                $scope.recentField = parseInt(snapshot.val()[$scope.settings_form_name].recentField) || null;
                $scope.currentField = parseInt(snapshot.val()[$scope.settings_form_name].currentField) || null;
                $scope.overdueField = parseInt(snapshot.val()[$scope.settings_form_name].overdueField) || null;

                $scope.contactForm = parseInt(snapshot.val()[$scope.settings_form_name].contactForm) || null;
                $scope.contactFormField = parseInt(snapshot.val()[$scope.settings_form_name].contactFormField) || null;
                
                $scope.approvalField = parseInt(snapshot.val()[$scope.settings_form_name].approvalField) || null;

                $scope.load_selected_form();
            } else {
                $scope.load_defaults();
            }

        })
    };

    $scope.load_selected_form = function() {
        if ($scope.selectedForm) {
            if ($scope.selectedForm && $scope.all_forms_ids.indexOf(parseInt($scope.selectedForm)) == -1) {
                $scope.selectedForm = $scope.all_forms[0].id;
            }
            for (var i in $scope.all_forms) {
                if ($scope.all_forms[i].id == $scope.selectedForm) {
                    $scope.selectedFormObject = $scope.all_forms[i];
                    for (var j in $scope.selectedFormObject.linkedForms) {
                        $scope.selectedFormObject.linkedForms[j].form.name = $scope.findFormName($scope.selectedFormObject.linkedForms[j].form.id);
                    }
                }
            }
            $scope.load_selected_form_fields();
        } else {
            $scope.load_defaults();
        }
    };

    $scope.load_defaults = function() {
        //$scope.selectedForm = $scope.all_forms[0].id;
        $scope.selectedForm = "1";

        $scope.selectedFormObject = $scope.all_forms[0];
        for (var j in $scope.selectedFormObject.linkedForms) {
            $scope.selectedFormObject.linkedForms[j].form.name = $scope.findFormName($scope.selectedFormObject.linkedForms[j].form.id);
        }
        $scope.load_selected_form_fields();

    };



    $scope.load_selected_form_fields = function() {
        $scope.all_form_fields_ids = [];
        return znData('FormFields').query({
            'formId': $scope.selectedForm
        }, {}, function(fields) {
            $scope.all_form_fields = [];
            angular.forEach(fields, function(field, index) {
                if (field.type == 'text' || field.type == 'text-input' || field.type == 'text-area' || field.type == 'linked') {
                    $scope.all_form_fields_ids.push(field.id);
                    $scope.all_form_fields.push(field);
                }
                if (field.id == $scope.nameField) {
                    $scope.title_placeholder = field.label;
                }
                if (field.id == $scope.seasonsField) {
                    $scope.description_placeholder = field.label;
                }
            });

        });
    };


    // Selected Form ID
    $scope.formId = 0;


    $scope.isDescendant = function(parent, child) {
        var node = child.parentNode;
        while (node != null) {
            if (node == parent) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    };


    $scope.selectedFormChanged = function() {

        angular.forEach($scope.all_forms, function(form) {
            if (form.id == parseInt($scope.selectedForm)) {

                $scope.selectedFormObject = form;
            }
        });

        for (var j in $scope.selectedFormObject.linkedForms) {
            $scope.selectedFormObject.linkedForms[j].form.name = $scope.findFormName($scope.selectedFormObject.linkedForms[j].form.id);
        }

    }

    $scope.save_plugin_settings_all = function() {
        $scope.loading_settings = true;
        var promises = [];
        promises = promises.concat($scope.save_plugin_settings('nameField', false));
        promises = promises.concat($scope.save_plugin_settings('seasonsField', false));
        promises = promises.concat($scope.save_plugin_settings('selectedForm', false));
        promises = promises.concat($scope.save_plugin_settings('assignToField', false));
        promises = promises.concat($scope.save_plugin_settings('amountField', false));
        promises = promises.concat($scope.save_plugin_settings('valueField', false));
        promises = promises.concat($scope.save_plugin_settings('lastActivityField', false));
        promises = promises.concat($scope.save_plugin_settings('followUpField', false));
        promises = promises.concat($scope.save_plugin_settings('notesDateField', false));
        promises = promises.concat($scope.save_plugin_settings('notesURLField', false));
        promises = promises.concat($scope.save_plugin_settings('notesEngagementField', false));
        promises = promises.concat($scope.save_plugin_settings('notesNotesField', false));
        promises = promises.concat($scope.save_plugin_settings('notesSubjectField', false));
        promises = promises.concat($scope.save_plugin_settings('notesFormField', false));
        promises = promises.concat($scope.save_plugin_settings('stateField', false));
        promises = promises.concat($scope.save_plugin_settings('field1Field', false));
        promises = promises.concat($scope.save_plugin_settings('field2Field', false));
        
        promises = promises.concat($scope.save_plugin_settings('field3Field', false));
        promises = promises.concat($scope.save_plugin_settings('grantFormField', false));
        
      


        promises = promises.concat($scope.save_plugin_settings('recentField', false));
        promises = promises.concat($scope.save_plugin_settings('currentField', false));
        promises = promises.concat($scope.save_plugin_settings('overdueField', false));

        promises = promises.concat($scope.save_plugin_settings('contactForm', false));
        promises = promises.concat($scope.save_plugin_settings('contactFormField', false));
        
        promises = promises.concat($scope.save_plugin_settings('approvalField', false));
        

        $q.all(promises).then(function() {
            znMessage('Settings saved, now refreshing!', 'saved');
            $window.location.reload();
        });
    };

    $scope.save_plugin_settings = function(fieldname, reload) {
        var promises = [];
        var formSettings;
        //$scope.name_field = $scope.settings_fields_array[0];
        //$scope.value_field = $scope.settings_fields_array[1];
        var count = 0;

        pluginSettings.once('value', function(snapshot) {
            if (snapshot) {
                formSettings = snapshot.val()[$scope.settings_form_name];
            }
        })

        //Firebase update Settings
        if ($scope[fieldname] === undefined) {
            $scope[fieldname] = '';
        }
        var temp_object = {};
        temp_object[fieldname] = $scope[fieldname];
        pluginSettings.child($scope.settings_form_name).update(temp_object);

    };

    return $scope.init();

}])