plugin.controller('wgnAddInetraction', ['$scope', 'znData', 'znModal', 'znMessage', function($scope, znData, znModal, znMessage) {

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: false
    };
    $scope.opened = false;
    $scope.follow_up = function(date_string) {
        var opportunity_post = {};
        var contacts_post = {};
        var contact_id = $scope.record['field' + $scope.rootScope.stateField].id || false;
        
        var opportunity_field_name = 'field' + $scope.rootScope.opportunityFollowUpField;

        opportunity_post[opportunity_field_name] = date_string;
        contacts_post[contact_field_name] = date_string;
        var contacts_form = $scope.rootScope.grantFormField;
        var opportunity_form = $scope.rootScope.form.id;
        if (contact_id) {

            znData('FormRecords').save({
                    formId: contacts_form,
                    id: contact_id
                },
                contacts_post,
                function(response) {
                    znData('FormRecords').save({
                            formId: opportunity_form,
                            id: $scope.record.id
                        },
                        opportunity_post,
                        function(response) {
                            znMessage('Interaction added successfully', 'saved');
                            angular.element('.modal-footer').find('button').next().trigger('click');
                        },
                        function(e) {
                            znMessage('Error saving to opportunit object', 'error');

                        });
                },
                function(e) {
                    znMessage('Error saving to contact object', 'error');
                });
        } else {
            znData('FormRecords').save({
                    formId: opportunity_form,
                    id: $scope.record.id
                },
                opportunity_post,
                function(response) {
                    znMessage('Interaction added successfully', 'saved');
                    angular.element('.modal-footer').find('button').next().trigger('click');
                },
                function(e) {
                    znMessage('Error saving to opportunit object', 'error');
                });
        }
    }

    $scope.setBtnAction('Save', function(callback) {

        var post = {};
        post['field' + $scope.rootScope.notesSubjectField] = $scope.subject;
        post['field' + $scope.rootScope.notesNotesField] = $scope.notes;
        if ($scope.date && $scope.date != "") {
            var date = new Date();
             var yyyy = date.getFullYear().toString();
            var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = date.getDate().toString();
            var fulldate_string = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]); // padding
            post['field' + $scope.rootScope.notesDateField] = fulldate_string;

           
        }

        post['field' + $scope.rootScope.notesEngagementField] = $scope.record.id;
        znData('FormRecords').save({
            formId: $scope.rootScope.notesFormField
        }, post, function(response) {
             
            // $scopedump = 'Interaction added successfully';

            // znMessage('Interaction added successfully', 'saved');
            // angular.element('.modal-footer').find('button').next().trigger('click');
            
            
            
            var opportunity_form = $scope.rootScope.form.id;
            var opportunity_post = {};

            var date = new Date($scope.date.toString());
            opportunity_post['field' + $scope.rootScope.followUpField] = date.toJSON().split('-')[0] + '-' + date.toJSON().split('-')[1] + '-' + date.toString().replace(/\S+\s(\S+)\s(\d+)\s(\d+)\s.*/, '$2');
            $scope.subject = '';
            $scope.notes = '';
            $scope.date = '';

            //opportunity_post['field' + $scope.rootScope.followUpField] = fulldate_string;
            znData('FormRecords').save({
                    formId: opportunity_form,
                    id: $scope.record.id
                },
                opportunity_post,
                function(response) {
                    znMessage('Note added successfully', 'saved');
                    angular.element('.modal-footer').find('button').next().trigger('click');
                    //$scope.follow_up(post['field' + $scope.rootScope.notesDateField]);
                },
                function(e) {
                    znMessage('Error saving to opportunit object', 'error');

                });
            // znMessage('Note added successfully', 'saved');
            // angular.element('.modal-footer').find('button').next().trigger('click');

        }, function(e) {
            znMessage('Error moving record', 'error');
        });




    });
}])

.directive("wgnEventedDescription", [function() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            element.bind('keydown', function(e) {
                var code = e.keyCode || e.which;
                if (code === 13) {
                    e.preventDefault();
                    scope.save_field(attrs.group, attrs.folder, element.parent().find('div').prev().val(), element.val());
                    element.val('');
                    element.parent().find('div').prev().val('');
                }
            });
        }
    };
}])
plugin.directive("wgnEventedTitle", [function() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            element.bind('keydown', function(e) {
                var code = e.keyCode || e.which;
                if (code === 13) {
                    e.preventDefault();

                    element.parent().find('div').next().focus();
                }
            });
        }
    };
}])

plugin.directive('wgnUiMultiSortable', ['$parse', function($parse) {
    var options = {};
    angular.extend(options, {
        connectWith: "ul.records-container",
        items: "li.record",
        cancel: '.input-holder'
    });

    var ModelSynchronizer = function(uiElement, attrs) {

        var MODEL_SUBSET_ATTR = 'ui-sortable-model-subset';
        var INITIAL_POSITION_ATTR = 'ui-sortable-start-pos';
        var self = this;

        // Set some data-* attributes on element being sorted just before sorting starts
        this.appendDataOnStart = function() {
            uiElement.item.data(INITIAL_POSITION_ATTR, uiElement.item.index());
            uiElement.item.data(MODEL_SUBSET_ATTR, attrs.modelSubset);
        };

        // Update underlying model when elements sorted within one "sortable"
        this.updateSingleSortableModel = function(model) {
            _collectDataRequiredForModelSync();
            if (_isInternalUpdate() && _hasPositionChanged()) {
                _update(model);
            }
        };

        // Update underlying model when elements sorted between different "sortables"
        this.updateMultiSortableModel = function(model) {
            _collectDataRequiredForModelSync();
            _update(model);
        };

        function _collectDataRequiredForModelSync() {
            self.data = {
                origSubset: uiElement.item.data(MODEL_SUBSET_ATTR),
                destSubset: attrs.modelSubset,
                origPosition: uiElement.item.data(INITIAL_POSITION_ATTR),
                destPosition: uiElement.item.index()
            };
        }

        function _hasPositionChanged() {
            return (self.data.origPosition !== self.data.destPosition) || !_isInternalUpdate();
        }

        function _isInternalUpdate() {
            return attrs.modelSubset === undefined || self.data.origSubset === self.data.destSubset;
        }

        function _update(model) {
            if (attrs.modelSubset === undefined) {
                model.splice(self.data.destPosition, 0, model.splice(self.data.origPosition, 1)[0]);
            } else {
                //($parse(self.data.destSubset)(model)).splice(self.data.destPosition, 0, ($parse(self.data.origSubset)(model)).splice(self.data.origPosition, 1)[0]);
            }
        }
    };

    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            var opts = angular.extend({}, options, scope.$eval(attrs.uiOptions));
            if (ngModel !== null) {
                var _start = opts.start;

                opts.start = function(e, ui) {
                    new ModelSynchronizer(ui, attrs).appendDataOnStart();
                    _callUserDefinedCallback(_start)(e, ui);
                    return scope.$apply();
                };

                var _update = opts.update;
                opts.update = function(e, ui) {
                    _callUserDefinedCallback(_update)(e, ui);
                    return scope.$apply();
                };

                var _stop = opts.stop;
                opts.stop = function(e, ui) {
                    var modelSync = new ModelSynchronizer(ui, attrs);
                    modelSync.updateSingleSortableModel(ngModel.$modelValue);
                    _callUserDefinedCallback(_stop)(e, ui);
                    return scope.$apply();
                };

                var _receive = opts.receive;
                opts.receive = function(e, ui) {
                    var modelSync = new ModelSynchronizer(ui, attrs);
                    modelSync.updateMultiSortableModel(ngModel.$modelValue);
                    _callUserDefinedCallback(_receive)(e, ui);
                    return scope.$apply();
                };
            }

            function _callUserDefinedCallback(callback) {
                if (typeof callback === "function") {
                    return callback; // regular callback
                }
                if (typeof scope[callback] === "function") {
                    return scope[callback]; // $scope function as callback
                }
                return function() {}; // noop function
            }
            return element.sortable(opts);
        }
    };
}])

plugin.controller('wgnViewGrants', ['$scope', 'znData', 'znModal', '$templateCache','$location','znMessage', 
    function($scope, znData, znModal, $templateCache,$location,znMessage) {

    $scope.approveGrant = function(grant){

        var saveRequest = {
            formId: $scope.rootScope.grantFormField
        }
        
        var saveRecord = {
            id: grant.id
        };
        saveRecord[$scope.approvalFieldName] = grant[$scope.approvalFieldName];
        // saveRecord[$scope.approvalLinkFieldName] = grant.id;

        // if(grant.approval && grant.approval[$scope.approvalFieldName]){
        //     saveRecord.id = grant.approval.id;
        //     saveRecord[$scope.approvalFieldName] = grant.approval[$scope.approvalFieldName];
        // }

        znData('FormRecords').save(saveRequest, saveRecord, function(confirm){

            // grant.approval = confirm;
            $scope.allApproved = true;
            $scope.grants.forEach(function(lgrant){
                // if(lgrant.id == grant.id){grant[$scope.approvalFieldName] == 'Approved'}
                if(lgrant[$scope.approvalFieldName] !== 'Approved'){$scope.allApproved = false;}    
                // if(grant.approval[$scope.approvalFieldName] !== 'Approved'){$scope.allApproved = false;}    
            });
        }, function(e){
            znMessage('Not Saved - You may not have permission', 'error');
        });


    }

    $scope.approveAll = function(){
        $scope.grants.forEach(function(grant){
            grant[$scope.approvalFieldName] = 'Approved';
            $scope.approveGrant(grant);
        })
    }

    $scope.showGrant = function(record){
         $location.search('record', $scope.rootScope.grantFormField + '.' + record.id);
    };

    angular.forEach($scope.rootScope.all_forms,function(form){
        if(parseInt(form.id)==parseInt($scope.rootScope.grantFormField)){
            $scope.grant = form;
            angular.forEach($scope.grant.fields,function(field){
                if(parseInt(field.id)==parseInt($scope.rootScope.field1Field)){
                    $scope.field1Title = field.label;
                }
                if(parseInt(field.id)==parseInt($scope.rootScope.field2Field)){
                    $scope.field2Title = field.label;
                }
                if(parseInt(field.id)==parseInt($scope.rootScope.field3Field)){
                    $scope.field3Title = field.label;
                }
            });
        }
    });

    $scope.init = function() {
        
        var linked_record = 0;
        var link_player_int = 0;
        $scope.rootScope.all_forms.forEach(function(aform){
            if(aform.id==parseInt($scope.rootScope.selectedForm)){
                angular.forEach(aform.linkedForms,function(form){
                    if(form.form.id == parseInt($scope.rootScope.contactForm)){ 
                        link_player_int = "field"+form.keyField.id;
                    }
                });
            }
            if(aform.id==parseInt($scope.rootScope.grantFormField)){
                angular.forEach(aform.linkedForms,function(form){
                    if(form.form.id == parseInt($scope.rootScope.contactForm)){ //if the linked form is the player form....
                        linked_record = "field"+form.keyField.id;
                    }
                });
            }
        })
    
        var params = {};
        params['formId'] = $scope.rootScope.grantFormField;
        params['limit'] = 100;
        params['direction'] = 'desc';
        params['sort'] = 'id';
        params[linked_record] = $scope.record[link_player_int].id;
        
        $scope.field1name = "field"+$scope.rootScope.field1Field;
        $scope.field2name = "field"+$scope.rootScope.field2Field;
        $scope.field3name = "field"+$scope.rootScope.field3Field;
        $scope.approvalFieldName = 'field' + $scope.rootScope.approvalField;
        $scope.allApproved = true;
        
        var grantForm = $scope.rootScope.all_forms.filter(function(form){
            return form.id == $scope.rootScope.grantFormField;
        })[0];
        $scope.approvalFieldOptions = grantForm.fields.filter(function(field){
            return field.id == $scope.rootScope.approvalField;
        })[0].settings.properties.choices;

        znData('FormRecords').query(params).then(function(grants) {
            $scope.grants = grants;

            // I am being lazy and just going through every field, 
            // if any field has the record ID, then it is linked to the engagement
            // and we'll take it as a filtered record
            if($scope.shouldFilter){
                $scope.showTotal = true;
                $scope.grants = $scope.grants.filter(function(grant){
                    var isLinked = false;
                    angular.forEach(grant, function(field){
                        if(field && field.id && field.id == $scope.record.id){
                            isLinked = true;
                        }
                    });
                    return isLinked;
                });
            }
            
            $scope.total = 0;

            if($scope.grants && $scope.grants.length > 0){
                $scope.grants.forEach(function(grant){
                    $scope.total += Number(grant[$scope.field3name]);
                    if(grant[$scope.approvalFieldName] !== 'Approved'){$scope.allApproved = false;}
                });
            }
            
        });
    }
    $scope.init();
}])

plugin.controller('wgnViewInetractions', ['$scope', 'znData', 'znModal', '$templateCache','$location', function($scope, znData, znModal, $templateCache,$location) {
    
    $scope.setBtnAction('Add new note', function(callback) {
        znModal({
            title: 'My Modal Dialog',
            template: $templateCache.get('case-mgmt-stage-add-interaction.html'),
            scope: {
                'record': $scope.record,
                'rootScope': $scope.rootScope
            },
            classes: 'add-interaction-modal',
            btns: {
                'Save': {
                    primary: true
                }
            },
            unique: false
        });
    });
    $scope.init = function() {
        
        
         var linked_record = 0;
        angular.forEach($scope.rootScope.form.linkedForms,function(form){
            if(parseInt(form.form.id)==parseInt($scope.rootScope.notesFormField)){
                linked_record = "field"+form.keyField.id;
            }
        });
    
        var params = {};
        params['formId'] = $scope.rootScope.notesFormField;
        params['limit'] = 1000;
        params['direction'] = 'desc';
        params['sort'] = 'id';
        params[linked_record] = $scope.record.id;
        
        znData('FormRecords').query(params).then(function(interactions) {
            $scope.interactions = interactions;
        });

    }
    $scope.init();
    
    $scope.showNote = function(record){
         $location.search('record', $scope.rootScope.notesFormField + '.' + record.id);
    };
}])
plugin.controller('wgnViewOpportunity', ['$scope', 'znMessage', 'znData', 'znModal', '$templateCache', function($scope, znMessage, znData, znModal, $templateCache) {


    $scope.fields_object = {
        "opportunity_name": "field291162",
        "follow_up_date": "field342990",
        "follow_up_desc": "field381196",
        "rep": "field291172",
        "close_month": "field344669",
        "value": "field291166",
        "decision_date": "field381195",
        "go_live_date": "field381194",
        "fit": "field381197",
        "need": "field381193",
        "budget": "field381191",
        "process": "field381192",
        "contact": "field291164",
        "type": "field355565",
        "platform": "field359035",
        "description": "field292594",
        "notes": "field381198"
    };
    
    $scope.item = {};

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: false
    };
    $scope.opened = false;
    $scope.opened2 = false;
    $scope.opened3 = false;
    $scope.active_tab = 'qualification';

    angular.forEach($scope.rootScope.all_forms_fields, function(field) {
        //type field id
        if (field.form_id === $scope.rootScope.selectedFormObject.id && field.id === 355565) {
            $scope.type_settings = field;
        }
        // //platform field id
        if (field.form_id === $scope.rootScope.selectedFormObject.id && field.id === 359035) {
            $scope.platform_settings = field;
        }
    });

    znData('WorkspaceMembers').query({ workspaceId:$scope.rootScope.workspace_id }, function(members) {
        $scope.members = members;
    });

    for (var i in $scope.fields_object) {
        if ($scope.record[$scope.fields_object[i]] && angular.isObject($scope.record[$scope.fields_object[i]])) {
            if ($scope.record[$scope.fields_object[i]].name && $scope.record[$scope.fields_object[i]].name.indexOf(',') != -1) {
                if (i == 'rep') {
                    $scope.item[i] = $scope.record[$scope.fields_object[i]].id;
                } else {
                    var name = $scope.record[$scope.fields_object[i]].name.split(',');
                    $scope.item[i] = name[1] + " " + name[0];
                }
            } else {
                if (i == 'rep') {
                    $scope.item[i] = $scope.record[$scope.fields_object[i]].id;
                } else {
                    $scope.item[i] = $scope.record[$scope.fields_object[i]].name;
                }
            }

        } else if ($scope.record[$scope.fields_object[i]] && !angular.isObject($scope.record[$scope.fields_object[i]])) {
            $scope.item[i] = $scope.record[$scope.fields_object[i]];
        }
    }

    var parsedDate = function(date){
        if(!date){return date};
        var dateObj = {};
        if(typeof date == 'object'){
                date.setTime( date.getTime()  );
                date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                date = date.toJSON();
                dateObj.day = date.split('-')[2].split('T')[0];
        } else {
                dateObj.day = date.split('-')[2];
        }
        dateObj.month = date.split('-')[1];
        dateObj.year = date.split('-')[0];
        return dateObj;
    }

    $scope.setBtnAction('Save', function(callback) {
        var opportunity_post = {};
        for (var i in $scope.fields_object) {
            //if ($scope.record[$scope.fields_object[i]] && i != "contact") {
            if ($scope.record.hasOwnProperty($scope.fields_object[i]) && i != "contact") {
                if ($scope.item[i] && i == "follow_up_date") {
                    var date = parsedDate($scope.item[i]);
                    opportunity_post[$scope.fields_object[i]] = date.year + '-' + date.month + '-' + date.day;

                } else if ($scope.item[i] && i == "decision_date") {
                    // var date = new Date($scope.item[i].toString());
                    // opportunity_post[$scope.fields_object[i]] = date.toJSON().split('-')[0] + '-' + date.toJSON().split('-')[1] + '-' + date.toString().replace(/\S+\s(\S+)\s(\d+)\s(\d+)\s.*/, '$2');
                    var date = parsedDate($scope.item[i]);
                    opportunity_post[$scope.fields_object[i]] = date.year + '-' + date.month + '-' + date.day;

                } else if ($scope.item[i] && i == "go_live_date") {
                    // var date = new Date($scope.item[i].toString());
                    // opportunity_post[$scope.fields_object[i]] = date.toJSON().split('-')[0] + '-' + date.toJSON().split('-')[1] + '-' + date.toString().replace(/\S+\s(\S+)\s(\d+)\s(\d+)\s.*/, '$2');
                    var date = parsedDate($scope.item[i]);
                    opportunity_post[$scope.fields_object[i]] = date.year + '-' + date.month + '-' + date.day;

                } else if ($scope.item[i] && i != "contact") {
                    opportunity_post[$scope.fields_object[i]] = $scope.item[i];
                }
            }
        }
        
        for(var i in $scope.record){
           if(i.indexOf('field')!=-1 && !angular.isObject($scope.record[i])){
               $scope.record[i] = opportunity_post[i];
           }else if(i.indexOf('field')!=-1 && angular.isObject($scope.record[i])){
               $scope.record[i].id = opportunity_post[i];
           }
        }

        znData('FormRecords').save({
                formId: $scope.rootScope.selectedFormObject.id,
                id: $scope.record.id
            },
            opportunity_post,
            function(response) {
                znMessage('Opportunity updated successfully', 'saved');
                //angular.element('.modal-footer').find('button').next().trigger('click');
            },
            function(e) {
                znMessage('Error saving to opportunity object', 'error');

            });
    });


    $scope.init = function() {
        // This fuction has been removed as it seems unnecessary.
        // If for some reason it is needed, uncomment the call for $scope.init() below.
        angular.forEach($scope.rootScope.form.linkedForms, function(relation, key) {
            if (parseInt(relation.keyField.id) == parseInt($scope.rootScope.stateField)) {
                $scope.contact_form = relation.form.id;
            }
        });


        var params = {};
        params['formId'] = $scope.contact_form;
        params['limit'] = 1000;
        params['id'] = $scope.record['field' + $scope.rootScope.stateField].id;

        znData('FormRecords').query(params).then(function(contacts) {
            $scope.contact = contacts[0];
        });
    }
    //$scope.init();
}])

plugin.directive('wgnUiMultiSortable', ['$parse', function($parse) {
    var options = {};
    angular.extend(options, {
        connectWith: "ul.records-container",
        items: "li.record",
        cancel: '.input-holder'
    });

    var ModelSynchronizer = function(uiElement, attrs) {

        var MODEL_SUBSET_ATTR = 'ui-sortable-model-subset';
        var INITIAL_POSITION_ATTR = 'ui-sortable-start-pos';
        var self = this;

        // Set some data-* attributes on element being sorted just before sorting starts
        this.appendDataOnStart = function() {
            uiElement.item.data(INITIAL_POSITION_ATTR, uiElement.item.index());
            uiElement.item.data(MODEL_SUBSET_ATTR, attrs.modelSubset);
        };

        // Update underlying model when elements sorted within one "sortable"
        this.updateSingleSortableModel = function(model) {
            _collectDataRequiredForModelSync();
            if (_isInternalUpdate() && _hasPositionChanged()) {
                _update(model);
            }
        };

        // Update underlying model when elements sorted between different "sortables"
        this.updateMultiSortableModel = function(model) {
            _collectDataRequiredForModelSync();
            _update(model);
        };

        function _collectDataRequiredForModelSync() {
            self.data = {
                origSubset: uiElement.item.data(MODEL_SUBSET_ATTR),
                destSubset: attrs.modelSubset,
                origPosition: uiElement.item.data(INITIAL_POSITION_ATTR),
                destPosition: uiElement.item.index()
            };
        }

        function _hasPositionChanged() {
            return (self.data.origPosition !== self.data.destPosition) || !_isInternalUpdate();
        }

        function _isInternalUpdate() {
            return attrs.modelSubset === undefined || self.data.origSubset === self.data.destSubset;
        }

        function _update(model) {
            if (attrs.modelSubset === undefined) {
                model.splice(self.data.destPosition, 0, model.splice(self.data.origPosition, 1)[0]);
            } else {
                //($parse(self.data.destSubset)(model)).splice(self.data.destPosition, 0, ($parse(self.data.origSubset)(model)).splice(self.data.origPosition, 1)[0]);
            }
        }
    };

    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            var opts = angular.extend({}, options, scope.$eval(attrs.uiOptions));
            if (ngModel !== null) {
                var _start = opts.start;

                opts.start = function(e, ui) {
                    new ModelSynchronizer(ui, attrs).appendDataOnStart();
                    _callUserDefinedCallback(_start)(e, ui);
                    return scope.$apply();
                };

                var _update = opts.update;
                opts.update = function(e, ui) {
                    _callUserDefinedCallback(_update)(e, ui);
                    return scope.$apply();
                };

                var _stop = opts.stop;
                opts.stop = function(e, ui) {
                    var modelSync = new ModelSynchronizer(ui, attrs);
                    modelSync.updateSingleSortableModel(ngModel.$modelValue);
                    _callUserDefinedCallback(_stop)(e, ui);
                    return scope.$apply();
                };

                var _receive = opts.receive;
                opts.receive = function(e, ui) {
                    var modelSync = new ModelSynchronizer(ui, attrs);
                    modelSync.updateMultiSortableModel(ngModel.$modelValue);
                    _callUserDefinedCallback(_receive)(e, ui);
                    return scope.$apply();
                };
            }

            function _callUserDefinedCallback(callback) {
                if (typeof callback === "function") {
                    return callback; // regular callback
                }
                if (typeof scope[callback] === "function") {
                    return scope[callback]; // $scope function as callback
                }
                return function() {}; // noop function
            }
            return element.sortable(opts);
        }
    };
}])

plugin.controller('wgnViewInetractions', ['$scope', 'znData', 'znModal', '$templateCache','$location', function($scope, znData, znModal, $templateCache,$location) {
    
    $scope.setBtnAction('Add new note', function(callback) {
        znModal({
            title: 'My Modal Dialog',
            template: $templateCache.get('case-mgmt-stage-add-interaction.html'),
            scope: {
                'record': $scope.record,
                'rootScope': $scope.rootScope
            },
            classes: 'add-interaction-modal',
            btns: {
                'Save': {
                    primary: true
                }
            },
            unique: false
        });
    });
    $scope.init = function() {
        
        
         var linked_record = 0;
        angular.forEach($scope.rootScope.form.linkedForms,function(form){
            if(parseInt(form.form.id)==parseInt($scope.rootScope.notesFormField)){
                linked_record = "field"+form.keyField.id;
            }
        });
    
        var params = {};
        params['formId'] = $scope.rootScope.notesFormField;
        params['limit'] = 1000;
        params['direction'] = 'desc';
        params['sort'] = 'id';
        params[linked_record] = $scope.record.id;
        
        znData('FormRecords').query(params).then(function(interactions) {
            $scope.interactions = interactions;
        });

    }
    $scope.init();
    
    $scope.showNote = function(record){
         $location.search('record', $scope.rootScope.notesFormField + '.' + record.id);
    };
}])
plugin.controller('wgnViewOpportunity', ['$scope', 'znMessage', 'znData', 'znModal', '$templateCache', function($scope, znMessage, znData, znModal, $templateCache) {


    $scope.fields_object = {
        "opportunity_name": "field291162",
        "follow_up_date": "field342990",
        "follow_up_desc": "field381196",
        "rep": "field291172",
        "close_month": "field344669",
        "value": "field291166",
        "decision_date": "field381195",
        "go_live_date": "field381194",
        "fit": "field381197",
        "need": "field381193",
        "budget": "field381191",
        "process": "field381192",
        "contact": "field291164",
        "type": "field355565",
        "platform": "field359035",
        "description": "field292594",
        "notes": "field381198"
    };
    
    $scope.item = {};

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: false
    };
    $scope.opened = false;
    $scope.opened2 = false;
    $scope.opened3 = false;
    $scope.active_tab = 'qualification';

    angular.forEach($scope.rootScope.all_forms_fields, function(field) {
        //type field id
        if (field.form_id === $scope.rootScope.selectedFormObject.id && field.id === 355565) {
            $scope.type_settings = field;
        }
        // //platform field id
        if (field.form_id === $scope.rootScope.selectedFormObject.id && field.id === 359035) {
            $scope.platform_settings = field;
        }
    });

    znData('WorkspaceMembers').query({ workspaceId:$scope.rootScope.workspace_id }, function(members) {
        $scope.members = members;
    });

    for (var i in $scope.fields_object) {
        if ($scope.record[$scope.fields_object[i]] && angular.isObject($scope.record[$scope.fields_object[i]])) {
            if ($scope.record[$scope.fields_object[i]].name && $scope.record[$scope.fields_object[i]].name.indexOf(',') != -1) {
                if (i == 'rep') {
                    $scope.item[i] = $scope.record[$scope.fields_object[i]].id;
                } else {
                    var name = $scope.record[$scope.fields_object[i]].name.split(',');
                    $scope.item[i] = name[1] + " " + name[0];
                }
            } else {
                if (i == 'rep') {
                    $scope.item[i] = $scope.record[$scope.fields_object[i]].id;
                } else {
                    $scope.item[i] = $scope.record[$scope.fields_object[i]].name;
                }
            }

        } else if ($scope.record[$scope.fields_object[i]] && !angular.isObject($scope.record[$scope.fields_object[i]])) {
            $scope.item[i] = $scope.record[$scope.fields_object[i]];
        }
    }

    var parsedDate = function(date){
        if(!date){return date};
        var dateObj = {};
        if(typeof date == 'object'){
                date.setTime( date.getTime()  );
                date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                date = date.toJSON();
                dateObj.day = date.split('-')[2].split('T')[0];
        } else {
                dateObj.day = date.split('-')[2];
        }
        dateObj.month = date.split('-')[1];
        dateObj.year = date.split('-')[0];
        return dateObj;
    }

    $scope.setBtnAction('Save', function(callback) {
        var opportunity_post = {};
        for (var i in $scope.fields_object) {
            //if ($scope.record[$scope.fields_object[i]] && i != "contact") {
            if ($scope.record.hasOwnProperty($scope.fields_object[i]) && i != "contact") {
                if ($scope.item[i] && i == "follow_up_date") {
                    var date = parsedDate($scope.item[i]);
                    opportunity_post[$scope.fields_object[i]] = date.year + '-' + date.month + '-' + date.day;

                } else if ($scope.item[i] && i == "decision_date") {
                    // var date = new Date($scope.item[i].toString());
                    // opportunity_post[$scope.fields_object[i]] = date.toJSON().split('-')[0] + '-' + date.toJSON().split('-')[1] + '-' + date.toString().replace(/\S+\s(\S+)\s(\d+)\s(\d+)\s.*/, '$2');
                    var date = parsedDate($scope.item[i]);
                    opportunity_post[$scope.fields_object[i]] = date.year + '-' + date.month + '-' + date.day;

                } else if ($scope.item[i] && i == "go_live_date") {
                    // var date = new Date($scope.item[i].toString());
                    // opportunity_post[$scope.fields_object[i]] = date.toJSON().split('-')[0] + '-' + date.toJSON().split('-')[1] + '-' + date.toString().replace(/\S+\s(\S+)\s(\d+)\s(\d+)\s.*/, '$2');
                    var date = parsedDate($scope.item[i]);
                    opportunity_post[$scope.fields_object[i]] = date.year + '-' + date.month + '-' + date.day;

                } else if ($scope.item[i] && i != "contact") {
                    opportunity_post[$scope.fields_object[i]] = $scope.item[i];
                }
            }
        }
        
        for(var i in $scope.record){
           if(i.indexOf('field')!=-1 && !angular.isObject($scope.record[i])){
               $scope.record[i] = opportunity_post[i];
           }else if(i.indexOf('field')!=-1 && angular.isObject($scope.record[i])){
               $scope.record[i].id = opportunity_post[i];
           }
        }

        znData('FormRecords').save({
                formId: $scope.rootScope.selectedFormObject.id,
                id: $scope.record.id
            },
            opportunity_post,
            function(response) {
                znMessage('Opportunity updated successfully', 'saved');
                //angular.element('.modal-footer').find('button').next().trigger('click');
            },
            function(e) {
                znMessage('Error saving to opportunity object', 'error');

            });
    });


    $scope.init = function() {
        // This fuction has been removed as it seems unnecessary.
        // If for some reason it is needed, uncomment the call for $scope.init() below.
        angular.forEach($scope.rootScope.form.linkedForms, function(relation, key) {
            if (parseInt(relation.keyField.id) == parseInt($scope.rootScope.stateField)) {
                $scope.contact_form = relation.form.id;
            }
        });


        var params = {};
        params['formId'] = $scope.contact_form;
        params['limit'] = 1000;
        params['id'] = $scope.record['field' + $scope.rootScope.stateField].id;

        znData('FormRecords').query(params).then(function(contacts) {
            $scope.contact = contacts[0];
        });
    }
    //$scope.init();
}])