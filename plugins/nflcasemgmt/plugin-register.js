plugin.register('wgn', {
    route: '/wgn',
    title: 'NFL Case Mgmt',
    icon: 'icon-suitcase',
    interfaces: [
        {
            controller: 'wgnCntl',
            template: 'wgn-main',
            type: 'fullPage',
            order: 300,
            topNav: true,
            routes: [
                '/:page'
            ]
        },
        {
            controller: 'wgnSettingsCntl',
            template: 'wgn-settings',
            type: 'settings'
        }
    ]
}); 